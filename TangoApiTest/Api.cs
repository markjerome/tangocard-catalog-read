﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TangoApiTest.Providers.Tangocard;

namespace TangoApiTest
{
    public class Api
    {
        private readonly HttpClient _httpClient;

        public Api(string platformName, string platformKey)
        {
            _httpClient = new HttpClient();

            var byteArray = Encoding.ASCII.GetBytes($"{platformName}:{platformKey}");
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
        }

        public async Task<Catalog> GetCatalog()
        {
            var url = "https://integration-api.tangocard.com/raas/v2/catalogs?verbose=true";

            var response = await _httpClient.GetAsync(url);
            var content = response.Content;

            var json = await content.ReadAsStringAsync();

            return Newtonsoft.Json.JsonConvert.DeserializeObject<Catalog>(json);
        }
    }
}
