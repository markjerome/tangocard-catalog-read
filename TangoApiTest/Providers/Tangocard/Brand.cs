﻿using System.Collections.Generic;

namespace TangoApiTest.Providers.Tangocard
{
    public class Brand
    {
        public string BrandKey { get; set; }
        public string BrandName { get; set; }
        public string Description { get; set; }
        public string Disclaimer { get; set; }
        public BrandRequirements BrandRequirements { get; set; }
        public Dictionary<string,string> ImageUrls { get; set; }
        public string ShortDescription { get; set; }
        public string Terms { get; set; }
        public string Status { get; set; }
        public List<BrandItem> Items { get; set; }
        public string LastUpdateDate { get; set; }
        public string CreatedDate { get; set; }
    }
}
