﻿using System.Collections.Generic;

namespace TangoApiTest.Providers.Tangocard
{
    public class BrandItem
    {       
        public List<string> Countries { get; set; }
        public string CurrencyCode { get; set; }
        public double? FaceValue { get; set; }
        public double? MaxValue { get; set; }
        public double? MinValue { get; set; }
        public string RewardName { get; set; }
        public string RewardType { get; set; }
        public string UtId { get; set; }
        public string ValueType { get; set; }
        public string RedemptionInstructions { get; set; }
        public bool IsWholeAmountValueRequired { get; set; }
        public string Status { get; set; }
        public string LastUpdateDate { get; set; }
        public string ExchangeRateRule { get; set; }
        public string CreatedDate { get; set; }
        public List<string> CredentialTypes { get; set; }
    }
}
