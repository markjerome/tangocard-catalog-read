﻿namespace TangoApiTest.Providers.Tangocard
{
    public class BrandRequirements
    {
        public bool AlwaysShowDisclaimer { get; set; }
        public string DisclaimerInstructions { get; set; }
        public string DisplayInstructions { get; set; }
        public string TermsAndConditionsInstructions { get; set; }
    }
}
