﻿using System.Collections.Generic;

namespace TangoApiTest.Providers.Tangocard
{
    public class Catalog
    {
        public string CatalogName { get; set; }
        public List<Brand> Brands { get; set; }
    }
}
