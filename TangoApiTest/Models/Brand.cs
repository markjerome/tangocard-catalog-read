﻿using System.Collections.Generic;

namespace TangoApiTest.Models
{
    public class Brand
    {
        public Brand(string brandKey, string brandName, string description, string disclaimer, Dictionary<string, string> imageUrls, string shortDescription, string terms, string status, string createdDate, string lastUpdateDate, BrandRequirements requirements)
        {
            BrandKey = brandKey;
            BrandName = brandName;
            Description = description;
            Disclaimer = disclaimer;
            ImageUrls = imageUrls;
            ShortDescription = shortDescription;
            Terms = terms;
            Status = status;
            CreatedDate = createdDate;
            LastUpdateDate = lastUpdateDate;
            Requirements = requirements;
        }
     
        public string BrandKey { get; set; }
        public string BrandName { get; set; }
        public string Description { get; set; }
        public string Disclaimer { get; set; }
        public Dictionary<string,string> ImageUrls { get; set; }
        public string ShortDescription { get; set; }
        public string Terms { get; set; }
        public string Status { get; set; }
        public string CreatedDate { get; set; }
        public string LastUpdateDate { get; set; }
        public BrandRequirements Requirements { get; set; }
    }
}
