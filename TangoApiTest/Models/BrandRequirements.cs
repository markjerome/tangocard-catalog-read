﻿namespace TangoApiTest.Models
{
    public class BrandRequirements
    {
        public BrandRequirements(bool alwaysShowDisclaimer, string disclaimerInstructions, string displayInstructions, string termsAndConditionsInstructions)
        {
            AlwaysShowDisclaimer = alwaysShowDisclaimer;
            DisclaimerInstructions = disclaimerInstructions;
            DisplayInstructions = displayInstructions;
            TermsAndConditionsInstructions = termsAndConditionsInstructions;
        }

        public bool AlwaysShowDisclaimer { get; set; }
        public string DisclaimerInstructions { get; set; }
        public string DisplayInstructions { get; set; }
        public string TermsAndConditionsInstructions { get; set; }
    }
}
