﻿using System;
using System.Collections.Generic;

namespace TangoApiTest.Models
{
    public class Reward
    {
        public Reward(Guid rewardId, IVoucherProvider provider, Brand brand, List<string> countries, string currencyCode, double? faceValue, double? maxValue, double? minValue, string rewardName, string rewardType, string valueType, string redemptionInstructions, bool isWholeAmountValueRequired, string status, string lastUpdateDate, string exchangeRateRule, string createdDate, List<string> credentialTypes)
        {
            RewardId = rewardId;
            Provider = provider;
            Brand = brand;
            Countries = countries;
            CurrencyCode = currencyCode;
            FaceValue = faceValue;
            MaxValue = maxValue;
            MinValue = minValue;
            RewardName = rewardName;
            RewardType = rewardType;
            ValueType = valueType;
            RedemptionInstructions = redemptionInstructions;
            IsWholeAmountValueRequired = isWholeAmountValueRequired;
            Status = status;
            LastUpdateDate = lastUpdateDate;
            ExchangeRateRule = exchangeRateRule;
            CreatedDate = createdDate;
            CredentialTypes = credentialTypes;
        }

        public Guid RewardId { get; set; }
        public IVoucherProvider Provider { get; set; }
        public Brand Brand { get; set; }   
        public List<string> Countries { get; set; }
        public string CurrencyCode { get; set; }
        public double? FaceValue { get; set; }
        public double? MaxValue { get; set; }
        public double? MinValue { get; set; }
        public string RewardName { get; set; }
        public string RewardType { get; set; }
        public string ValueType { get; set; }
        public string RedemptionInstructions { get; set; }
        public bool IsWholeAmountValueRequired { get; set; }
        public string Status { get; set; }
        public string LastUpdateDate { get; set; }
        public string ExchangeRateRule { get; set; }
        public string CreatedDate { get; set; }
        public List<string> CredentialTypes { get; set; }
    }
}
