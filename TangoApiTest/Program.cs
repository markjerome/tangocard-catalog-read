﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using TangoApiTest.Models;
using TangoApiTest.Providers.Tangocard;
using TangoCard.Raas;
using TangoCard.Raas.Models;

namespace TangoApiTest
{
    class Program
    {
        private static Api Api;
        static void Main(string[] args)
        {
            Api = new Api("QAPlatform2","apYPfT6HNONpDRUj3CLGWYt7gvIHONpDRUYPfT6Hj");
            var tangoCatalog = Api.GetCatalog().Result;

            var tangoRewards = NormalizeTangocardCatalog(tangoCatalog);

            var tangoRewardsJson = Newtonsoft.Json.JsonConvert.SerializeObject(tangoRewards);

            Console.WriteLine(tangoRewardsJson);

            //var ukRewards = Query(tangoRewards, "GBP", "UK");

            //var ukRewardsJson = Newtonsoft.Json.JsonConvert.SerializeObject(ukRewards);

            Console.ReadKey();
        }

        static List<Reward> NormalizeTangocardCatalog(Catalog catalog)
        {
            var rewards = new List<Reward>();

            foreach (var b in catalog.Brands)
            {
                foreach (var i in b.Items)
                {
                    var id = $"{VoucherProviders.Tangocard}-{i.UtId}";
                    var rewardId = StringToGuid(id);

                    rewards.Add(new Reward(
                        rewardId,
                        VoucherProviders.Tangocard,
                        new Models.Brand(b.BrandKey, b.BrandName, b.Description, b.Disclaimer, b.ImageUrls, b.ShortDescription, b.Terms, b.Status, b.CreatedDate, b.LastUpdateDate, new Models.BrandRequirements(b.BrandRequirements.AlwaysShowDisclaimer, b.BrandRequirements.DisclaimerInstructions, b.BrandRequirements.DisplayInstructions, b.BrandRequirements.TermsAndConditionsInstructions)),
                        i.Countries,
                        i.CurrencyCode,
                        i.FaceValue,
                        i.MaxValue,
                        i.MinValue,
                        i.RewardName,
                        i.RewardType,
                        i.ValueType,
                        i.RedemptionInstructions,
                        i.IsWholeAmountValueRequired,
                        i.Status,
                        i.LastUpdateDate,
                        i.ExchangeRateRule,
                        i.CreatedDate,
                        i.CredentialTypes)
                   );
                }
            }

            return rewards;
        }

        static CatalogModel ReadCatalogFromSdk()
        {
            Configuration.PlatformName = "QAPlatform2";
            Configuration.PlatformKey = "apYPfT6HNONpDRUj3CLGWYt7gvIHONpDRUYPfT6Hj";
            Configuration.Environment = Configuration.Environments.SANDBOX;
            var client = new RaasClient();
            return client.Catalog.GetCatalog();
        }

        static List<Reward> Query(List<Reward> rewards, string currency, string country)
        {
            return rewards.Where(x =>
                x.Countries.Contains(country) &&
                x.CurrencyCode == currency
            ).ToList();
        }

        static Guid StringToGuid(string value)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create();
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
            return new Guid(data);
        }
    }
}
