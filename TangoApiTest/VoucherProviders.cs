﻿using System;
using System.Collections.Generic;
using System.Linq;
using TangoApiTest.Providers;

namespace TangoApiTest
{
    public static class VoucherProviders
    {
        public static IVoucherProvider Tangocard = new Tangocard();

        public static IVoucherProvider Parse(string value)
        {
            var country = All().FirstOrDefault(x => x.Id == value.ToUpper());
            if (country != null)
                return country;

            throw new ArgumentOutOfRangeException(nameof(value), value, "Unable to match country");
        }

        public static bool TryParse(string id, out IVoucherProvider country)
        {
            try
            {
                country = Parse(id);
                return true;
            }
            catch (ArgumentOutOfRangeException)
            {
                country = null;
                return false;
            }
        }

        public static List<IVoucherProvider> All()
        {
            return new List<IVoucherProvider>()
            {
                Tangocard
            };
        }
    }
}
