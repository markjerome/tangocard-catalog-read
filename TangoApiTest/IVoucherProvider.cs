﻿namespace TangoApiTest
{
    public interface IVoucherProvider
    {
        string Id { get; }
        string Name { get; }
    }
}
