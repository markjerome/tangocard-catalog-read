﻿namespace TangoApiTest
{
    public class Tangocard : IVoucherProvider
    {
        public string Id => "TANGOCARD";
        public string Name => "Tango Card";

        public override string ToString() => Id;
    }
}
